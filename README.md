# alert_system

## Credit
* First, I want to give credit where credit is due. W3Schools had an awesome tutorial on using node.js and websocket.io with the raspberry pi. It was a great template that lead me in the right direction. Here is the link.  https://www.w3schools.com/nodejs/nodejs_raspberrypi_webserver_websocket.asp

> The goal of this project was to create an alert system by the push of a button.
When the physical button on the raspberry pi is pressed,
a text is sent to your phone that someone is at the door,
music or sound is emitted through the speakers connected to the computer (locally and remotely),
and a notification pops up on your computer screen that someone is at your door.

> This is all in real time without having to refresh your computer screen by using socket.io along with node.js.

> This could be used for many different situations.
My first thought was for contacting 911 if a person cannot call. Another option is for the elderly when they live alone, kind of like
the alert systems they already use.
Another option was for the hearing impaired, the vibration of the text on their phone will let them know someone is at the door.

> I created this program with html, python, and javaScript.